from django.urls import path
from accounts.models import Account


class ModelForm(ModelForm):
    class Meta:
        model = Account
        fields = ["username", "password"]

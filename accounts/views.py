from django.shortcuts import render, redirect
from .models import UserLoginForm


# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

        if username == "your_username" and password == "your_password":

            return redirect("success")

        else:
            return render(
                request,
                "login.html",
                {"form": form, "error": "Invalid username or password."},
            )
    else:
        form = UserLoginForm()

    return render(request, "login.html", {"form": form})
